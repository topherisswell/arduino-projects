#define TRIG_PIN 12
#define ECHO_PIN 13


void setup() 
{
  //initialize serial communications at a 9600 baud rate
  Serial.begin(9600);

  // set pin modes
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

}

void loop()
{
  // should be low already, but for consistency's sake
  digitalWrite(TRIG_PIN, LOW);  
  delayMicroseconds(2); 

  // send 10 us ping (min recommended by the datasheet)
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10); 
  digitalWrite(TRIG_PIN, LOW);

  // the time that the echo pin is high is the length of the echo
  // Round trip time (RTT) is in microseconds
  // using longs to reduce the (unlikely) chance of overflows
  long rtt_in_us = pulseIn(ECHO_PIN, HIGH);

  // convert to centimeters based on assumed 343 m/s speed of sound
  // distance = RTT(us) * 343 m/s * 1/2 * (1 s / 1e6 us) * 100 cm / 1 m
  long distance_in_cm = rtt_in_us / 58.3;

  // if it's outside of the operating parameters specified in the datasheet,
  // discard the results as unreliable. 
  if (distance_in_cm > 400 || distance_in_cm < 2){
    Serial.println("Unable to determine range");
  }
  else {
    Serial.print(distance_in_cm);
    Serial.println(" cm");
  }

  // wait half a second before measuring again.
  delay(500);
}
